package UAP;

abstract class TiketKonser implements HargaTiket {
    // Variabel untuk menyimpan nama dan harga tike konser
    protected String nama;
    protected double harga;

    public TiketKonser(String nama, double harga) {
    // Inisialisasi variabel nama dan harga dari parameter    
        this.nama = nama;
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public abstract double hitungHarga();
}