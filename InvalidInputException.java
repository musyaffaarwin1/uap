package UAP;

//Digunakan untuk menandakan bahwa terjadi kesalahan pada input dalam konteks pemesanan tiket konser
// Konstruktor "InvalidInputException" menerima parameter "message" yang akan diteruskan ke konstruktor superclass "Exception" menggunakan kata kunci "super()".
class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
}
