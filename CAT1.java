package UAP;

//Subclass dari TiketKonser
class CAT1 extends TiketKonser {
    public CAT1(String nama, double harga) {
        super(nama, harga);
    }
// Method hitung harga yang di override dengan mengembalikan nilai harga tiket tanpa adanya perubahan
    @Override
    public double hitungHarga() {
        return harga;
    }
}