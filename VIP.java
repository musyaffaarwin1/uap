package UAP;

class VIP extends TiketKonser {
    public VIP(String nama, double harga) {
        super(nama, harga);
    }

    // Method hitung harga yang di override dengan mengembalikan nilai harga tiket tanpa adanya perubahan
    @Override
    public double hitungHarga() {
        return harga;
    }
}