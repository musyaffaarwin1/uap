package UAP;

/* Array "tiketKonser" adalah variabel statis yang menyimpan objek-objek tiket konser yang tersedia untuk dipilih.
 * Melalui kelas "PemesananTiket", kita dapat memperoleh objek tiket konser yang sudah tersedia dengan memanggil metode "pilihTiket()" dan memberikan indeks tiket yang diinginkan.
 */
class PemesananTiket {
    private static TiketKonser[] tiketKonser;

    static {
        tiketKonser = new TiketKonser[]{
            new CAT8("CAT 8", 1000000),
            new CAT1("CAT 1", 2500000),
            new FESTIVAL("FESTIVAL", 6000000),
            new VIP("VIP", 8000000),
            new VVIP("UNLIMITED EXPERIENCE", 14000000)
        };
    }

    public static TiketKonser pilihTiket(int index) {
        return tiketKonser[index];
    }
}
