package UAP;

//Subclass dari TiketKonser
class FESTIVAL extends TiketKonser {
    public FESTIVAL(String nama, double harga) {
        super(nama, harga);
    }
// Method hitung harga yang di override dengan mengembalikan nilai harga tiket tanpa adanya perubahan
    @Override
    public double hitungHarga() {
        return harga;
    }
}