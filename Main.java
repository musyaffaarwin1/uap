/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */
package UAP;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        doPemesananTiket();
    }

    public static void doPemesananTiket() {
        int pilihan;
        Scanner scanner = new Scanner(System.in);
        System.out.println("===========================================");
        System.out.println("Selamat datang di Pemesanan Tiket Coldplay!");
        System.out.println("===========================================");
        try {
            // Untuk Memasukkan nama pemesan sebelum lanjut ke program selanjutnya
            System.out.print("Masukkan Nama Anda Untuk Melanjutkan Pemesanan Tiket : ");
            String nama = scanner.nextLine();

            // Menampilkan Pilihan Jenis Tiket
            System.out.println("Silahkan Memilih Jenis Tiket Yang Anda Inginkan : ");
            System.out.println("1. CAT 8");
            System.out.println("2. CAT 1");
            System.out.println("3. FESTIVAL");
            System.out.println("4. VIP");
            System.out.println("5. UNLIMITED EXPERIENCE");
            System.out.print("Masukkan pilihan: ");

            try {
                pilihan = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                throw new InvalidInputException("ERROR : Masukkan pilihan tiket dengan angka.");
            }
            // Memvalidasi Pilihan Tiket Yang Dimasukkan Pengguna
            // Jika Pengguna Memasukkan Pilihan Lebih Kecil dari 1 or lebih besar dari 5
            // Maka Output nya akan mengeluarkan Pesan Input tidak valid

            if (pilihan < 1 || pilihan > 5) {
                throw new InvalidInputException("ERROR : Pilihan hanya 1-5.");
            }

            // Memilih tiket berdasarkan pilihan pengguna
            TiketKonser tiket = PemesananTiket.pilihTiket(pilihan - 1);

            // Untuk Menampilkan kode Booking secara Acak
            // Dan Menampilkan Tanggal Pemesanan Terkini

            String kodeBooking = generateKodeBooking();
            String tanggalPesanan = getCurrentDate();

            // Detail Pemesanan Tiket

            System.out.println(" ================== Detail Pemesanan ================== ");
            System.out.println("Nama Pemesan: " + nama);
            System.out.println("Kode Booking: " + kodeBooking);
            System.out.println("Tanggal Pesanan: " + tanggalPesanan);
            System.out.println("Tiket yang dipesan: " + tiket.getNama());
            System.out.println("Total harga: Rp. " + tiket.hitungHarga());
            System.out.println(" ====================================================== ");

        } catch (InvalidInputException e) {
            System.out.println("Terjadi kesalahan: " + e.getMessage());
            doPemesananTiket(); // Repeat the process
        } catch (Exception e) {
            System.out.println("Terjadi kesalahan: " + e.getMessage());
            doPemesananTiket(); // Repeat the process
        }
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */

    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}